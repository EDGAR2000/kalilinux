package banquito;

public class Usuario {
	
	/* en esta clase declaramos todos los datos que vamos a necesitar y les generamos sus set y getters
	 * para que se puedan utilizar fuera de su clase*/
	private int id; 
	private String nombre;
	private String apellP;
	private String apellM;
	private int NIP;
	private String tarjeta;
	private float fondos;
	private int FechaN;
	private String nombre2;
	
	public int getFechaN() {
		return FechaN;
	}
	public void setFechaN(int fechaN) {
		FechaN = fechaN;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellP() {
		return apellP;
	}
	public void setApellP(String apellP) {
		this.apellP = apellP;
	}
	public String getApellM() {
		return apellM;
	}
	public void setApellM(String apellM) {
		this.apellM = apellM;
	}
	public int getNIP() {
		return NIP;
	}
	public void setNIP(int nIP) {
		NIP = nIP;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public float getFondos() {
		return fondos;
	}
	public void setFondos(float fondos) {
		this.fondos = fondos;
	}
	public String getNombre2() {
		return nombre2;
	}
	public void setNombre2(String nombre2) {
		this.nombre2 = nombre2;
	}
	// este sirve para convertir los datos en un string y se visualizen en consola
	public String toString() {
		return "Usuario Tarjeta=" + tarjeta +"\n NIP= "+ NIP +"\n Fondos=" + fondos+"\n Nombre Cliente= "+ nombre +"\n tutor=" + nombre2;
	}
	
	public float fondo() {
		return fondos;
	}

}
