package banquito;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

public class RegistroUs {
// esta clase sirve como conexion a nuestra base de datos
	
	private ObjectContainer db = null;
	// sirve para que se empiezen a escribir los datos
	private void abrirRegistro() {
		db = Db4oEmbedded.openFile("Registro usuario");
	}
	// cuando ya acabo de llenarse los datos y se puedan guardar
	private void cerrarRegistro() {
		db.close();
	}
	/* metodo que sirve para ingresar los nuevos usuarios */
	public void insertarRegistro(Usuario u) {
		
		abrirRegistro();// importante abrir
		db.store(u);
		cerrarRegistro();// importante cerrar ya que si no cerramos generara error la base de datos
	}
	public Usuario seleccionarPersona(Usuario u) {
		abrirRegistro();//abrir
		ObjectSet resultado = db.queryByExample(u);
		Usuario usuario = (Usuario) resultado.next();
		cerrarRegistro();
		return usuario;//cerrar
	}
	
	// cuando el usuario pueda actualizar su dinero*
	public void actualizarFondo(float fondo, String id) {
		abrirRegistro();
		Usuario u = new Usuario();
		u.setTarjeta(id);
		
		ObjectSet resultado = db.queryByExample(u);
		Usuario preresultado = (Usuario) resultado.next();
		preresultado.setFondos(fondo);
		db.store(preresultado);
		cerrarRegistro();		
	}
	// meotdo que actualiza  el nip
	public void actualizarNip(String id, int nip) {
		abrirRegistro();// abrir
		Usuario u = new Usuario();
		u.setTarjeta(id);
		ObjectSet resultado = db.queryByExample(u);
		Usuario preresultado = (Usuario) resultado.next();
		preresultado.setNIP(nip);
		db.store(preresultado);
		cerrarRegistro();// cerar
	}
	
}
