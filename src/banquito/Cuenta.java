package banquito;

public class Cuenta {
	private String tarjeta;
	private float saldo;
	private int nip;
	//instancia de la clase RegistroUs
	RegistroUs con = new RegistroUs();
	
	//cargamos el contructor con los datos que esperamos recibir
	public Cuenta(String cuenta, float fondo, int dato) {
		tarjeta = cuenta;
		saldo = fondo;
		nip = dato;
	}
	 //funcion que se llama al principal sirve para agregar dinero a la cuenta
	public void depositar(float dinero) {
		// cuando la funcion recibe el parametro del dinero este se suma al saldo que tengas
		saldo = saldo + dinero;
		con.actualizarFondo(saldo, tarjeta);
		}
	public void retirar(float dinero) {
		
		// este if sirve por si el monto es mayor a la que tiene la tarjeta mande este error
		if(dinero>saldo) {
			saldo = Integer.parseInt("Error Ponga un monto adecuado");
			
		}else {
			// de  lo contrario si es bueno al monto se le reste
			saldo = saldo - dinero;
			con.actualizarFondo(saldo, tarjeta);
		}
		
	}
	public Usuario fondos() {
		/* esta funcion sirve para que cuando el usuario quiera saber su fondo consulte a la bd
		 * cuanto tiene 
		*/
		Usuario user = new Usuario();
		user.setTarjeta(tarjeta);
		user = con.seleccionarPersona(user);
		
		return user;
	}
	public float saldo() {
		return saldo;
	}
	/* esta funcion sirve para actualizar los datos de la nip le pasamos los parametros que
	 * necesitmaos para la actualizacion*/
	public void actualizar(int dato) {
		nip=dato;
		con.actualizarNip(tarjeta, nip);
		
	}
	
	
	
}
